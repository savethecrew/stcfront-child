<?php
function stc_enqueue_styles() {

    $parent_style = 'storefront-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'stc_enqueue_styles' );

/**
 * Display the site title or logo
 *
 * @since 2.1.0
 * @param bool $echo Echo the string or return it.
 * @return string
 */
function storefront_site_title_or_logo( $echo = true ) {
    if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
        $logo = get_custom_logo_savethecrew();
        $html = is_home() ? '<h1 class="logo">' . $logo . '</h1>' : $logo;
    } elseif ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
        // Copied from jetpack_the_site_logo() function.
        $logo    = site_logo()->logo;
        $logo_id = get_theme_mod( 'custom_logo' ); // Check for WP 4.5 Site Logo
        $logo_id = $logo_id ? $logo_id : $logo['id']; // Use WP Core logo if present, otherwise use Jetpack's.
        $size    = site_logo()->theme_size();
        $html    = sprintf( '<a href="%1$s" class="site-logo-link" rel="home" itemprop="url">%2$s</a>',
            esc_url( 'https://savethecrew.com' ),
            wp_get_attachment_image(
                $logo_id,
                $size,
                false,
                array(
                    'class'     => 'site-logo attachment-' . $size,
                    'data-size' => $size,
                    'itemprop'  => 'logo'
                )
            )
        );

        $html = apply_filters( 'jetpack_the_site_logo', $html, $logo, $size );
    } else {
        $tag = is_home() ? 'h1' : 'div';

        $html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( 'https://savethecrew.com' ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) .'>';

        if ( '' !== get_bloginfo( 'description' ) ) {
            $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
        }
    }

    if ( ! $echo ) {
        return $html;
    }

    echo $html;
}

/**
 * Customized get_custom_logo function because I'm having a hard time with apply_filters()
 *
 * @since 2.1.0
 * @param int $blog_id The blog ID, on multisite
 * @return string
 */
function get_custom_logo_savethecrew( $blog_id = 0 ) {
    $html = '';
    $switched_blog = false;

    if ( is_multisite() && ! empty( $blog_id ) && (int) $blog_id !== get_current_blog_id() ) {
        switch_to_blog( $blog_id );
        $switched_blog = true;
    }

    $custom_logo_id = get_theme_mod( 'custom_logo' );

    // We have a logo. Logo is go.
    if ( $custom_logo_id ) {
        $custom_logo_attr = array(
            'class'    => 'custom-logo',
            'itemprop' => 'logo',
        );

        /*
         * If the logo alt attribute is empty, get the site title and explicitly
         * pass it to the attributes used by wp_get_attachment_image().
         */
        $image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );
        if ( empty( $image_alt ) ) {
            $custom_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
        }

        /*
         * If the alt attribute is not empty, there's no need to explicitly pass
         * it because wp_get_attachment_image() already adds the alt attribute.
         */
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" rel="home" itemprop="url">%2$s</a>',
            esc_url( 'https://savethecrew.com' ),
            wp_get_attachment_image( $custom_logo_id, 'full', false, $custom_logo_attr )
        );
    }

    // If no logo is set but we're in the Customizer, leave a placeholder (needed for the live preview).
    elseif ( is_customize_preview() ) {
        $html = sprintf( '<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
            esc_url( 'https://savethecrew.com' )
        );
    }

    if ( $switched_blog ) {
        restore_current_blog();
    }

    /**
     * Filters the custom logo output.
     *
     * @since 4.5.0
     * @since 4.6.0 Added the `$blog_id` parameter.
     *
     * @param string $html    Custom logo HTML output.
     * @param int    $blog_id ID of the blog to get the custom logo for.
     */
    return apply_filters( 'get_custom_logo', $html, $blog_id );
}

?>